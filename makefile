lib.o: lib.asm
	nasm -f elf64 -o lib.o lib.asm
	
lib2.o: lib.inc lib.o
	nasm -f elf64 -o lib2.o lib.inc
	
colon.o: colon.inc
	nasm -f elf64 -o colon.o colon.inc

dict.o: lib2.o dict.asm
	nasm -f elf64 -o dict.o dict.asm
	
main.o: colon.o dict.o words.inc main.asm
	nasm -f elf64 -o main.o main.asm
	
program: lib.o lib2.o colon.o dict.o main.o
	ld -o program lib.o lib2.o main.o colon.o dict.o
	
