global string_equals
global read_char
global read_word
global read_line
global parse_uint
global parse_int
global string_copy
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global print_error

section .text


; takes a pointer to a null-terminated string, prints it to stderr with a newline
print_error:
    call string_length
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 2
    syscall
    call print_newline
    ret

; Accepts a return code and terminates the current process
exit:
    mov rax, rdi
    xor rdi, rdi
    syscall

; Takes a pointer to a null-terminated string, returns its length
string_length:
    mov rax, 0
    .loop:
      xor rax, rax
    .count:
      cmp byte [rdi+rax], 0
      je .end
      inc rax
      jmp .count
    .end:
      ret

; Takes a pointer to a null-terminated string, prints it to stdout
print_string:
    call string_length
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, 1
    mov  rdi, 1
    syscall
    ret

; Takes a character code and writes it to stdout
print_char:
    push rdi
     mov  rax, 1
     mov  rdi, 1
     mov  rsi, rsp
     mov  rdx, 1
     syscall
     pop rax
     ret

; Translates a string (outputs character with code 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Outputs an unsigned 8-byte number in decimal format
print_uint:
    mov rax, rdi
    mov r8, 10
    mov r9, 0
   .div_loop:
        xor rdx, rdx
        div r8
        add rdx, '0'
        push rdx
        inc r9
        test rax, rax
        jne .div_loop

    .print_loop:
        pop rdi
        call print_char
        dec r9
        test r9, r9
        jne .print_loop
    ret

; Outputs a signed 8-byte number in decimal format
print_int:
    print_int:
    cmp rdi, 0
    jge .print
    neg rdi
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    .print:
        call print_uint
        ret

; Takes two pointers to null-terminated strings, returns 1 if they are equal, 0 otherwise
string_equals:
    call string_length
    mov r8, rax
    push rdi
    mov rdi, rsi
    call string_length
    pop rdi
    cmp rax, r8
    jne .fail

    xor rax, rax
    .loop:
        mov r8b, byte[rsi+rax]
        cmp byte[rdi+rax], r8b
        jne .fail
        cmp byte [rdi + rax], 0
        je .ok
        inc rax
        jmp .loop
    .ok:
        mov rax, 1
        ret
    .fail:
        mov rax, 0
        ret


; Reads one character from stdin and returns it. Returns 0 if the end of the stream has been reached
read_char:
    push 0
     mov  rax, 0
     mov  rdi, 0
     mov  rsi, rsp
     mov  rdx, 1
     syscall
     pop rax
     ret

; Accepts: buffer start address, buffer size
; Reads a word from stdin into the buffer, skipping leading whitespace, Whitespace characters are space 0x20, tab 0x9 and newline 0xA.
; Stops and returns 0 if the word is too big for the buffer
; On success, returns the address of the buffer in rax, the length of the word in rdx, Returns 0 to rax on failure
; This function should add a null terminator to the word

read_line:
    mov r9, rsi
    xor r8, r8
    .loop:
        push rdi
        call read_char
        pop rdi

        cmp rax, 0x20
        je .check
        cmp rax, 0x9
        je .check
        cmp rax, 0xA
        je .check

    .continue:
        cmp rax, 0
        je .ok

        cmp r8, r9
        je .fail

        mov byte[rdi + r8], al
        inc r8
        jmp .loop

    .check:
        test r8, r8
        je .loop
        cmp rax, 0x20 ;checking for a space in the middle of a word
        je .continue
    .ok:
        mov rax, rdi
        mov rdx, r8
        mov byte[rdi + r8], 0
        inc r8
        ret
    .fail:
        xor rax, rax
        ret

; Takes a pointer to a string, tries
; read from its beginning an unsigned number. Returns in rax: number, rdx : its length in characters, rdx = 0 if the number could not be read
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10

    .loop:
        xor r9, r9
        mov r9b, byte[rdi]

        cmp r9b, '0'
        jl .end
        cmp r9b, '9'
        jg .end

        inc rcx
        mul r8
        sub r9b, '0'
        add rax, r9
        inc rdi
        jmp .loop
    .end:
        mov rdx, rcx
        ret


; Takes a pointer to a string, tries
; read from its beginning a signed number.
; If there is a sign, spaces between it and the number are not allowed.
; Returns in rax: the number, rdx : its length in characters (including the sign, if any)
; rdx = 0 if the number could not be read
parse_int:
    xor rax, rax
    mov al, byte[rdi]
    cmp al, '-'
    je .sign
    call parse_uint
    jmp .end
    .sign:
        inc rdi
        call parse_uint
        test rdx, rdx
        je .end
        inc rdx
        neg rax
     .end:
        ret

; It takes a pointer to a string, a pointer to a buffer, and a length of the buffer.
; Copies a string to a buffer
; Returns the length of the string if it fits in the buffer, otherwise 0
string_copy:
    push rdi
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    cmp rax, rdx
    jg .fail
    xor rax, rax
    xor r8, r8
    .loop:
        mov r8b,  byte[rdi + rax]
        mov byte[rsi + rax], r8b
        cmp r8b, 0
        je .ok
        inc rax
        jmp .loop
    .ok:
        ret
    .fail:
        xor rax, rax
        ret
