%define NEXT 0

%macro colon 2
    %%current_address: dq NEXT
    %define NEXT %%current_address
    db %1, 0
    %2:
%endmacro
